<!DOCTYPE html>
<html>
	<head>

		<title>
                     Pinger
                </title>

                
                    <link rel="icon" href="/img/favicon.ico" type="image/x-icon" />
                    <script src="/vendor/foundation-5.4.7/js/vendor/jquery.js" /></script>
                    <script src="/vendor/foundation-5.4.7/js/foundation.min.js" /></script>
                    <link rel="stylesheet" href="/vendor/foundation-5.4.7/css/foundation.css" />
                    <script src="/js/pinger.js" /></script>
                    <link rel="stylesheet" href="/css/pinger.css" type="text/css" />
                    <link rel="stylesheet" href="/vendor/foundation-5.4.7/img/foundation-icons/foundation-icons.css" type="text/css" />
                

	</head>
	<body>
            <div class="row">
            <div class="large-12 columns">
                <h1>Pinger</h1>
            <div class="icon-bar five-up"> 
                <a class="item" href="/">
                    <i class="fi-home"></i>
                    <label>Home</label> 
                </a> 

                <a class="item" href="/projects/search">
                    <i class="fi-list"></i>
                    <label>List</label> 
                </a> 

                <a class="item" href="/projects/new">
                    <i class="fi-page"></i>
                    <label>New</label> 
                </a> 
            </div>
            </div>
            </div>

            <div class="row">
            <div class="large-12 columns">
		<?php echo $this->getContent(); ?>
            </div>
            </div>

            <div class="row">
            <div class="large-12 columns">
            <div>
                &copy; Copyright TMW Ltd
            </div>
            </div>
            </div>
	</body>
</html>