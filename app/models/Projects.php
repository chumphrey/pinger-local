<?php

use Phalcon\Mvc\Model\Validator\Uniqueness,
    Phalcon\Mvc\Model\Validator\Regex as RegexValidator;

class Projects extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $stack;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $purpose;

    /**
     *
     * @var string
     */
    public $outcome;

    /**
     *
     * @var string
     */
    public $created;

    /**
     *
     * @var string
     */
    public $modified;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $client;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }      

    public function getCreated()
    {
        return $this->created;
    }
    
    public function setCreated($created)
    {
        $this->created = $created;
    }    

    public function getModified()
    {
        return $this->modified;
    }
    
    public function setModified($modified)
    {
        $this->modified = $modified;
    }
    
    public function validation()
    {
        $this->validate(new Uniqueness(
            array(
                "field"   => "name",
                "message" => "The project name must be unique"
            )
        ));

        $this->validate(new RegexValidator(array(
            'field' => 'url',
            'pattern' => "#((http|https)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#",
            'message' => 'The URL should be a valid legal using http:// or https:// protocol scheme'
        )));
      
        return $this->validationHasFailed() != true;
    }
}
