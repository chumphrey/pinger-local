<?php

# Default username and password
$user = 'root';
$pass = 'root';
$db   = 'pinger';

# Default user and pass for non local
if($_SERVER['SERVER_NAME']==='pinger-ci.tmwtest.co.uk'){
    ini_set("display_errors",0);
    $user = 'pinger-ci';    
    $pass = '7qENzqXrGKxCqdCz'; 
}

# Default user and pass for non local
if($_SERVER['SERVER_NAME']==='pinger-qa.tmwtest.co.uk'){
    ini_set("display_errors",0);    
    $user = 'pinger-qa'; // have to add this because of this error
    
    /*

            Error executing 'REVOKE SELECT, 
            INSERT, UPDATE, DELETE, CREATE, 
            DROP, GRANT OPTION, REFERENCES, 
            INDEX, ALTER, CREATE TEMPORARY TABLES, 
            LOCK TABLES, CREATE VIEW, SHOW VIEW, 
            CREATE ROUTINE, ALTER ROUTINE, EXECUTE, 
            EVENT, TRIGGER ON `pinger`.* 

            FROM 'pinger-qa'@'%%''
      
            There is no such grant defined for user 'pinger-qa' on host '%%'.
            SQL Error: 1141
     */
    
    $pass = '7qENzqXrGKxCqdCz';
}

# Default user and pass for non local
if($_SERVER['SERVER_NAME']==='pinger-local'){
    $db   = 'pingr_deployment';
}

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => $user,
        'password'    => $pass,
        'dbname'      => $db,
    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'baseUri'        => '/',
    )
));
