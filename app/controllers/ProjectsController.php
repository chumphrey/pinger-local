<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator,
    Phalcon\Db\RawValue;

class ProjectsController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->response->redirect('projects/search');
    }

    /**
     * Searches for projects
     */
    public function searchAction()
    {

        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Projects", $this->request->getPost() );
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = array();
        }
        $parameters["order"] = "id";

        $projects = Projects::find($parameters);
        if ($projects->count() == 0) {
            $this->flash->notice("The search did not find any projects");

            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "index"
            ));
        }

        $paginator = new Paginator(array(
            "data" => $projects,
            "limit"=> 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Edits a project
     *
     * @param string $id
     */
    public function editAction($id)
    {

        if (!$this->request->isPost()) {

            $project = Projects::findFirstByid($id);
            if (!$project) {
                $this->flash->error("project was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "projects",
                    "action" => "index"
                ));
            }

            $this->tag->setDefault("id", $project->id);
            $this->tag->setDefault("name", $project->name);
            $this->tag->setDefault("stack", $project->stack);
            $this->tag->setDefault("url", $project->url);
            $this->tag->setDefault("description", $project->description);
            $this->tag->setDefault("purpose", $project->purpose);
            $this->tag->setDefault("outcome", $project->outcome);
            $this->tag->setDefault("created", $project->created);
            $this->tag->setDefault("modified", date('Y-m-d h:i:s'));
            $this->tag->setDefault("status", $project->status);
            $this->tag->setDefault("client", $project->client);
            
        }
    }
    
    /**
     * Views a project
     *
     * @param string $id
     */
    public function viewAction($id)
    {
        if (!$this->request->isPost()) {

            $project = Projects::findFirstByid($id);
            if (!$project) {
                $this->flash->error("project was not found");

                return $this->dispatcher->forward(array(
                    "controller" => "projects",
                    "action" => "index"
                ));
            }

            $this->view->id = $project->id;
            $this->tag->setDefault("id", $project->id);
            $this->tag->setDefault("name", $project->name);
            $this->tag->setDefault("stack", $project->stack);
            $this->tag->setDefault("url", $project->url);
            $this->tag->setDefault("description", $project->description);
            $this->tag->setDefault("purpose", $project->purpose);
            $this->tag->setDefault("outcome", $project->outcome);
            $this->tag->setDefault("created", $project->created);
            $this->tag->setDefault("modified", $project->modified);
            $this->tag->setDefault("status", $project->status);
            $this->tag->setDefault("client", $project->client);
            
        }
    }    

    /**
     * Saves a project edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "index"
            ));
        }

        $id = $this->request->getPost("id");

        $project = Projects::findFirstByid($id);
        if (!$project) {
            $this->flash->error("project does not exist " . $id);

            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "index"
            ));
        }

        $project->id          = $this->request->getPost("id");
        $project->name        = $this->request->getPost("name");
        $project->stack       = $this->request->getPost("stack");
        $project->url         = $this->request->getPost("url");
        $project->description = $this->request->getPost("description");
        $project->purpose     = $this->request->getPost("purpose");
        $project->outcome     = $this->request->getPost("outcome");
        $project->created     = $this->request->getPost("created");
        $project->modified    = $this->request->getPost("modified");
        $project->status      = $this->request->getPost("status");
        $project->client      = $this->request->getPost("client");


        if (!$project->save()) {

            foreach ($project->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "edit",
                "params" => array($project->id)
            ));
        }

        $this->flash->success("project was updated successfully");

        return $this->dispatcher->forward(array(
            "controller" => "projects",
            "action" => "index"
        ));
    }

    /**
     * Deletes a project
     *
     * @param string $id
     */
    public function deleteAction($id)
    {

        $project = Projects::findFirstByid($id);
        if (!$project) {
            $this->flash->error("project was not found");

            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "index"
            ));
        }

        if (!$project->delete()) {

            foreach ($project->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "search"
            ));
        }

        $this->flash->success("project was deleted successfully");

        return $this->dispatcher->forward(array(
            "controller" => "projects",
            "action" => "index"
        ));
    }
    
    /**
     * Displayes the creation form
     */
    public function newAction()
    {
        $lastRow = Projects::findFirst(['order' => 'id DESC'])->toArray();
        $this->view->setVar('id', $lastRow['id'] + 1);
    }
    
    /**
     * Creates a new project
     */
    public function createAction()
    {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "index"
            ));
        }

        $project = new Projects();        
        $project->id = $this->request->getPost("id");
        $project->name = $this->request->getPost("name");
        $project->stack = $this->request->getPost("stack");
        $project->url = $this->request->getPost("url");
        $project->description = $this->request->getPost("description");
        $project->purpose = $this->request->getPost("purpose");
        $project->outcome = $this->request->getPost("outcome");
        $project->created = new RawValue('now()');
        $project->modified = new RawValue('now()');
        $project->status = $this->request->getPost("status");
        $project->client = $this->request->getPost("client");

        if (!$project->save()) {
            foreach ($project->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(array(
                "controller" => "projects",
                "action" => "new"
            ));
        }

        $this->flash->success("project was created successfully");

        return $this->dispatcher->forward(array(
            "controller" => "projects",
            "action" => "index"
        ));

    }
    
    /**
     * status: 
     * 
     */    
    public function statusAction() {        
        // Vars
        $status = array();
        
        // DB qry
        $projects = $this->db->fetchAll('SELECT id, url FROM projects WHERE url != ""');

        // Check if method exists
        if(function_exists('system'))
        {
            // Get Project                    
            foreach($projects as $project)
            {  
                $ping = exec('ping -c 1 ' . parse_url($project['url'])['host'], $output);

                $status[] = array(
                            'id'   => $project['id'],
                            'ping' => explode('/',$ping)[5],                    
                        );
            }
        }
        
        $this->response->setJsonContent($status);
        $this->response->send();
        die;
    }
  
}