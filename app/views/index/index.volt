<h1>Welcome to Pinger!</h1>

<h2>What is pinger?</h2>

<ul>
    <li>Pinger is a phalconphp PoC (Proof of Concept)</li>
    <li>Pings a TMW project domain and returns the response</li>
    <li>It can be used to see if domains are available or unavailable</li>
    <li>CRUD Application (Create, Reuse, Update, Delete projects)</li>
</ul>

<h2>What pinger is not?</h2>
<ul>
<li><p>A Full Featured Project Management System</p></li>
</ul>

<p><b>Frontend</b></p>

<ul>
<li><a href="http://jquery.com/" target="_blank">JQuery v2.1.1</a></li>
<li><a href="http://foundation.zurb.com/" target="_blank">Foundation 5.4.7</a></li>
</ul>

<p><b>Backend</b></p>
<ul>
<li><a href="http://www.php.net" target="_blank">PHP</a></li>
<li><a href="http://www.phalconphp.com" target="_blank">Phalconphp 1.3.0</a> / <a href="http://docs.phalconphp.com/en/latest/reference/volt.html" target="_blank">Volt 1.3.0</a></li>
<li><a href="http://www.apache.org/" target="_blank">Apache 2</a></li>
<li><a href="http://www.mysql.com" target="_blank">MySQL 5</a></li>
<li><a href="http://www.mamp.info/en/" target="_blank">MAMP3</a></li>
</ul>