/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    // Vars
    var projects = [];
    
    // Gets URLs
    var projectsData = $('table.status a.projectURL');
    
    $(projects).each(function(i, e){
        var project = {
            id:  $(e).attr('id'),
            url: $(e).attr('href')
        };
        projects.push(project);
    });
    
    $.ajax({
        url: '/projects/status',
        method: 'POST',
        complete: function(res)
        {
            resultParsed = $.parseJSON(res.responseText);

            $(resultParsed).each(function(i, e){
                if(e.ping){
                $('#project_status_' + e.id).text(e.ping);
                }
            });
        },
        data: {
            projects: projects
        }
    });
});